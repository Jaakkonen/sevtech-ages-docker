FROM alpine:latest

#ADD https://media.forgecdn.net/files/2788/614/SevTech_Ages_Server_3.1.2-hotfix.1.zip /sevtechages.zip
RUN wget -O server.zip https://media.forgecdn.net/files/2788/614/SevTech_Ages_Server_3.1.2-hotfix.1.zip && \
    mkdir /server && \
    unzip server.zip -d /server && \
    apk add openjdk8 && \
    echo eula=true > /server/eula.txt

WORKDIR /server
RUN sh Install.sh && \
    mkdir /server/world

EXPOSE 25565

VOLUME world /server/world

ENTRYPOINT ["sh", "ServerStart.sh"]

